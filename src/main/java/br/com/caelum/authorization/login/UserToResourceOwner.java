package br.com.caelum.authorization.login;

import br.com.caelum.authorization.domain.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserToResourceOwner implements Converter<User,ResourceOwner> {
    @Override
    public ResourceOwner convert(User user) {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(user.getRoles());
        return new ResourceOwner(user.getUsername(), user.getPassword(), authorities);
    }
}
